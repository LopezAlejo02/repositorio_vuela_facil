/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios.repositorio;

import com.example.vuelafacil.backend.Clientes;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author FELIPE
 */
public interface ClientesDAO extends CrudRepository<Clientes, Integer> {
    
}
