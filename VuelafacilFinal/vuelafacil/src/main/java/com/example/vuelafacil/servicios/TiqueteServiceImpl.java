/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios;

import com.example.vuelafacil.backend.Tiquete;
import org.springframework.stereotype.Service;
import com.example.vuelafacil.interfaz.TiqueteServicio;
import com.example.vuelafacil.servicios.repositorio.TiqueteDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FELIPE
 */
@Service
public class TiqueteServiceImpl implements TiqueteServicio{
    
    @Autowired
    private TiqueteDAO tiqueteDao;

    @Override
    @Transactional (readOnly = false)
    public Tiquete save(Tiquete tiquete) {
        return tiqueteDao.save(tiquete);
    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
        tiqueteDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Tiquete findById(Integer id) {
        return tiqueteDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Tiquete> findAll() {
        return (List<Tiquete>) tiqueteDao.findAll();
    }
    
    
}
