/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.backend;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author FELIPE
 */
@Entity
@Table(name="puntosclientes")
public class PuntosClientes implements Serializable{
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="idTransaccion")
        private Integer idTransaccion;
    
        @ManyToOne
        @JoinColumn(name="idCliente")
        private Clientes idClientes;
        
        @ManyToOne
        @JoinColumn(name="nombreCliente")
        private Clientes nombreClientes;
        
        @ManyToOne
        @JoinColumn(name="puntosCompra")
        private Tiquete puntosCompras;
        
        @Column(name="puntosAcumulados")
        private Integer puntosAcumulados;

    public PuntosClientes() {
    }

    public PuntosClientes(Integer idTransaccion, Clientes idClientes, Clientes nombreClientes, Tiquete puntosCompras, Integer puntosAcumulados) {
        this.idTransaccion = idTransaccion;
        this.idClientes = idClientes;
        this.nombreClientes = nombreClientes;
        this.puntosCompras = puntosCompras;
        this.puntosAcumulados = puntosAcumulados;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Clientes getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(Clientes idClientes) {
        this.idClientes = idClientes;
    }

    public Clientes getNombreClientes() {
        return nombreClientes;
    }

    public void setNombreClientes(Clientes nombreClientes) {
        this.nombreClientes = nombreClientes;
    }

    public Tiquete getPuntosCompras() {
        return puntosCompras;
    }

    public void setPuntosCompras(Tiquete puntosCompras) {
        this.puntosCompras = puntosCompras;
    }

    public Integer getPuntosAcumulados() {
        return puntosAcumulados;
    }

    public void setPuntosAcumulados(Integer puntosAcumulados) {
        this.puntosAcumulados = puntosAcumulados;
    }
        
        
}
