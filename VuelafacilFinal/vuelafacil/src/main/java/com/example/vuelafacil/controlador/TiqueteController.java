/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.controlador;


import com.example.vuelafacil.backend.Tiquete;
import com.example.vuelafacil.interfaz.TiqueteServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FELIPE
 */
@RestController
@CrossOrigin ("*")
@RequestMapping ("/tiquete")
public class TiqueteController {
    
    @Autowired
    private TiqueteServicio tiqueteServicio;
    
    @PostMapping (value="/")
    public ResponseEntity<Tiquete>agregar(@RequestBody Tiquete tiquete){
        Tiquete obj=tiqueteServicio.save(tiquete);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping (value="/list/{id}")
    public ResponseEntity<Tiquete> eliminar(@PathVariable Integer id){
        Tiquete obj = tiqueteServicio.findById(id);
        if(obj!=null)
            tiqueteServicio.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Tiquete> editar(@RequestBody Tiquete tiquete){
        Tiquete obj = tiqueteServicio.findById(tiquete.getIdTiquete());
        if(obj!=null)
        {
            obj.setIdClientes(tiquete.getIdClientes());
            obj.setNombreClientes(tiquete.getNombreClientes());
            obj.setIdVuelos(tiquete.getIdVuelos());
            obj.setNombreRutas(tiquete.getNombreRutas());
            obj.setFechaHora(tiquete.getFechaHora());
            obj.setPuntosCompra(tiquete.getPuntosCompra());
            obj.setPuntosAcumuladosClientes(tiquete.getPuntosAcumuladosClientes());
            obj.setValorVuelo(tiquete.getValorVuelo());
                    
        tiqueteServicio.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Tiquete> consultarTodo(){
    return tiqueteServicio.findAll();
    }
    @GetMapping("/list/{id}")
    public Tiquete consultaPorId(@PathVariable Integer id){
    return tiqueteServicio.findById(id);
    }   
    
}
