/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios;

import com.example.vuelafacil.backend.Vuelos;
import com.example.vuelafacil.interfaz.VuelosServicio;
import com.example.vuelafacil.servicios.repositorio.VuelosDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FELIPE
 */
@Service
public class VuelosServiceImpl implements VuelosServicio{
    
    @Autowired
    private VuelosDAO vuelosDao;

    @Override
    @Transactional (readOnly = false)
    public Vuelos save(Vuelos vuelos) {
        return vuelosDao.save(vuelos);
    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
        vuelosDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Vuelos findById(Integer id) {
        return vuelosDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Vuelos> findAll() {
        return (List<Vuelos>) vuelosDao.findAll();
    }
    
    
}
