/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.controlador;

import com.example.vuelafacil.backend.PuntosClientes;
import com.example.vuelafacil.interfaz.PuntosClientesServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FELIPE
 */
@RestController
@CrossOrigin ("*")
@RequestMapping ("/tiquete")
public class PuntosClientesController {
    
    @Autowired
    private PuntosClientesServicio  puntosClientesServicio;
    
     @PostMapping (value="/")
    public ResponseEntity<PuntosClientes>agregar(@RequestBody PuntosClientes puntosclientes){
        PuntosClientes obj=puntosClientesServicio.save(puntosclientes);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping (value="/list/{id}")
    public ResponseEntity<PuntosClientes> eliminar(@PathVariable Integer id){
        PuntosClientes obj = puntosClientesServicio.findById(id);
        if(obj!=null)
            puntosClientesServicio.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/list/{id}")
    public ResponseEntity<PuntosClientes> editar(@RequestBody PuntosClientes puntosclientes){
        PuntosClientes obj = puntosClientesServicio.findById(puntosclientes.getIdTransaccion());
        if(obj!=null)
        {
            obj.setIdClientes(puntosclientes.getIdClientes());
            obj.setNombreClientes(puntosclientes.getNombreClientes());
            obj.setPuntosCompras(puntosclientes.getPuntosCompras());
            obj.setPuntosAcumulados(puntosclientes.getPuntosAcumulados());
            
                    
        puntosClientesServicio.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<PuntosClientes> consultarTodo(){
    return puntosClientesServicio.findAll();
    }
    @GetMapping("/list/{id}")
    public PuntosClientes consultaPorId(@PathVariable Integer id){
    return puntosClientesServicio.findById(id);
    }   
    
}
