/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.interfaz;

import com.example.vuelafacil.backend.HistoriaCliente;
import java.util.List;

/**
 *
 * @author FELIPE
 */
public interface HistoriaClienteServicio {
    public HistoriaCliente save(HistoriaCliente historiacliente);
    public void delete(Integer id);
    public HistoriaCliente findById(Integer id);
    public List<HistoriaCliente> findAll();
}
