/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.interfaz;

import com.example.vuelafacil.backend.Clientes;
import java.util.List;

/**
 *
 * @author FELIPE
 */
public interface ClienteServicio {
    public Clientes save(Clientes clientes);
    public void delete(Integer id);
    public Clientes findById(Integer id);
    public List<Clientes> findAll();

    
}

 