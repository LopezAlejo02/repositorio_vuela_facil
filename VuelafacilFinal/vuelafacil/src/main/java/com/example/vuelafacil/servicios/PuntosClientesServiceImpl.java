/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios;

import com.example.vuelafacil.backend.PuntosClientes;
import com.example.vuelafacil.interfaz.PuntosClientesServicio;
import com.example.vuelafacil.servicios.repositorio.PuntosClientesDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FELIPE
 */
@Service
public class PuntosClientesServiceImpl implements PuntosClientesServicio{
    
    @Autowired
    private PuntosClientesDAO PuntosClientesDao;

    @Override
    @Transactional (readOnly = false)
    public PuntosClientes save(PuntosClientes puntosClientes) {
        return PuntosClientesDao.save(puntosClientes);
    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
        PuntosClientesDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public PuntosClientes findById(Integer id) {
        return PuntosClientesDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<PuntosClientes> findAll() {
        return (List<PuntosClientes>) PuntosClientesDao.findAll();
    }
    
    
    
}
