/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.controlador;

import com.example.vuelafacil.backend.Clientes;
import com.example.vuelafacil.interfaz.ClienteServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FELIPE
 */
@RestController
@CrossOrigin ("*")
@RequestMapping ("/clientes")
public class ClientesController {
    
    @Autowired
    private ClienteServicio clienteServicio;
    
    @PostMapping (value="/")
    public ResponseEntity<Clientes>agregar(@RequestBody Clientes clientes){
        Clientes obj=clienteServicio.save(clientes);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    
    @DeleteMapping (value="/list/{id}")
    public ResponseEntity<Clientes> eliminar(@PathVariable Integer id){
        Clientes obj = clienteServicio.findById(id);
        if(obj!=null)
            clienteServicio.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    
    }
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Clientes> editar(@RequestBody Clientes clientes){
        Clientes obj = clienteServicio.findById(clientes.getIdCliente());
        if(obj!=null)
        {
            obj.setNombreCliente(clientes.getNombreCliente());
            obj.setTelefono(clientes.getTelefono());
        clienteServicio.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
}
@GetMapping("/list")
public List<Clientes> consultarTodo(){
return clienteServicio.findAll();
}
@GetMapping("/list/{id}")
public Clientes consultaPorId(@PathVariable Integer id){
return clienteServicio.findById(id);
}   
    
}
