/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.backend;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author FELIPE
 */
@Entity
@Table(name="tiquete")
public class Tiquete implements Serializable{
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="idTiquete")
        private Integer idTiquete;
        
        @ManyToOne
        @JoinColumn(name="idCliente")
        private Clientes idClientes;
        
        @ManyToOne
        @JoinColumn(name="nombreCliente")
        private Clientes nombreClientes;
        
        @ManyToOne
        @JoinColumn(name="idVuelo")
        private Vuelos idVuelos;
        
        @ManyToOne
        @JoinColumn(name="nombreRuta")
        private Rutas nombreRutas;
        
        @ManyToOne
        @JoinColumn(name="puntosAcumulados")
        private PuntosClientes puntosAcumuladosClientes;
        
        @Column(name="fechaHora")
        private String fechaHora;
        
        @Column(name="puntosCompra")
        private Integer puntosCompra;
        
        @Column(name="valorVuelo")
        private Integer valorVuelo;

    public Tiquete() {
    }

    public Tiquete(Integer idTiquete, Clientes idClientes, Clientes nombreClientes, Vuelos idVuelos, Rutas nombreRutas, PuntosClientes puntosAcumuladosClientes, String fechaHora, Integer puntosCompra, Integer valorVuelo) {
        this.idTiquete = idTiquete;
        this.idClientes = idClientes;
        this.nombreClientes = nombreClientes;
        this.idVuelos = idVuelos;
        this.nombreRutas = nombreRutas;
        this.puntosAcumuladosClientes = puntosAcumuladosClientes;
        this.fechaHora = fechaHora;
        this.puntosCompra = puntosCompra;
        this.valorVuelo = valorVuelo;
    }

    public Integer getIdTiquete() {
        return idTiquete;
    }

    public void setIdTiquete(Integer idTiquete) {
        this.idTiquete = idTiquete;
    }

    public Clientes getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(Clientes idClientes) {
        this.idClientes = idClientes;
    }

    public Clientes getNombreClientes() {
        return nombreClientes;
    }

    public void setNombreClientes(Clientes nombreClientes) {
        this.nombreClientes = nombreClientes;
    }

    public Vuelos getIdVuelos() {
        return idVuelos;
    }

    public void setIdVuelos(Vuelos idVuelos) {
        this.idVuelos = idVuelos;
    }

    public Rutas getNombreRutas() {
        return nombreRutas;
    }

    public void setNombreRutas(Rutas nombreRutas) {
        this.nombreRutas = nombreRutas;
    }

    public PuntosClientes getPuntosAcumuladosClientes() {
        return puntosAcumuladosClientes;
    }

    public void setPuntosAcumuladosClientes(PuntosClientes puntosAcumuladosClientes) {
        this.puntosAcumuladosClientes = puntosAcumuladosClientes;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Integer getPuntosCompra() {
        return puntosCompra;
    }

    public void setPuntosCompra(Integer puntosCompra) {
        this.puntosCompra = puntosCompra;
    }

    public Integer getValorVuelo() {
        return valorVuelo;
    }

    public void setValorVuelo(Integer valorVuelo) {
        this.valorVuelo = valorVuelo;
    }
    
        
}
