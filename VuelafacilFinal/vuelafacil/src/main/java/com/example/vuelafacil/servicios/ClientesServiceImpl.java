/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios;

import com.example.vuelafacil.backend.Clientes;
import com.example.vuelafacil.servicios.repositorio.ClientesDAO;
import com.example.vuelafacil.interfaz.ClienteServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FELIPE
 */
@Service
public class ClientesServiceImpl implements ClienteServicio {
    
    @Autowired
    private ClientesDAO clienteDao;

    @Override
    @Transactional (readOnly = false)
    public Clientes save(Clientes clientes) {
        return clienteDao.save(clientes);

    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
        clienteDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Clientes findById(Integer id) {
        return clienteDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Clientes> findAll() {
        return (List<Clientes>) clienteDao.findAll();
    }
    
}
