/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.controlador;

import com.example.vuelafacil.backend.Rutas;
import com.example.vuelafacil.interfaz.RutasServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FELIPE
 */
@RestController
@CrossOrigin ("*")
@RequestMapping ("/rutas")
public class RutasController {
    
    @Autowired
    private RutasServicio rutasServicio;
    
    @PostMapping (value="/")
    public ResponseEntity<Rutas>agregar(@RequestBody Rutas rutas){
        Rutas obj=rutasServicio.save(rutas);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping (value="/list/{id}")
    public ResponseEntity<Rutas> eliminar(@PathVariable Integer id){
        Rutas obj = rutasServicio.findById(id);
        if(obj!=null)
            rutasServicio.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
     @PutMapping(value="/list/{id}")
     public ResponseEntity<Rutas> editar(@RequestBody Rutas rutas){
        Rutas obj = rutasServicio.findById(rutas.getIdRuta());
        if(obj!=null)
        {
            obj.setNombreRuta(rutas.getNombreRuta());
        rutasServicio.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
     @GetMapping("/list")
    public List<Rutas> consultarTodo(){
    return rutasServicio.findAll();
    }
    @GetMapping("/list/{id}")
    public Rutas consultaPorId(@PathVariable Integer id){
    return rutasServicio.findById(id);
    }   
}
