/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.backend;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author FELIPE
 */
@Entity
@Table(name="historiacliente")
public class HistoriaCliente implements Serializable{
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="idHistoria")
        private Integer idHistoria;
        
        @ManyToOne
        @JoinColumn(name="idCliente")
        private Clientes idClientes;
        
        @ManyToOne
        @JoinColumn(name="nombreCliente")
        private Clientes nombreClientes;
        
        @ManyToOne
        @JoinColumn(name="nombreRuta")
        private Rutas nombreRutas;
        
        @ManyToOne
        @JoinColumn(name="fechaHora")
        private Tiquete fechaHoraTiquete;
        
        @ManyToOne
        @JoinColumn(name="valorVuelo")
        private Tiquete valorVueloTiquete;
        
        @ManyToOne
        @JoinColumn(name="puntosCompra")
        private Tiquete puntosCompras;
        
        @ManyToOne
        @JoinColumn(name="idTiquete")
        private Tiquete idTiquetes;

    public HistoriaCliente() {
    }

    public HistoriaCliente(Integer idHistoria, Clientes idClientes, Clientes nombreClientes, Rutas nombreRutas, Tiquete fechaHoraTiquete, Tiquete valorVueloTiquete, Tiquete puntosCompras, Tiquete idTiquetes) {
        this.idHistoria = idHistoria;
        this.idClientes = idClientes;
        this.nombreClientes = nombreClientes;
        this.nombreRutas = nombreRutas;
        this.fechaHoraTiquete = fechaHoraTiquete;
        this.valorVueloTiquete = valorVueloTiquete;
        this.puntosCompras = puntosCompras;
        this.idTiquetes = idTiquetes;
    }

    public Integer getIdHistoria() {
        return idHistoria;
    }

    public void setIdHistoria(Integer idHistoria) {
        this.idHistoria = idHistoria;
    }

    public Clientes getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(Clientes idClientes) {
        this.idClientes = idClientes;
    }

    public Clientes getNombreClientes() {
        return nombreClientes;
    }

    public void setNombreClientes(Clientes nombreClientes) {
        this.nombreClientes = nombreClientes;
    }

    public Rutas getNombreRutas() {
        return nombreRutas;
    }

    public void setNombreRutas(Rutas nombreRutas) {
        this.nombreRutas = nombreRutas;
    }

    public Tiquete getFechaHoraTiquete() {
        return fechaHoraTiquete;
    }

    public void setFechaHoraTiquete(Tiquete fechaHoraTiquete) {
        this.fechaHoraTiquete = fechaHoraTiquete;
    }

    public Tiquete getValorVueloTiquete() {
        return valorVueloTiquete;
    }

    public void setValorVueloTiquete(Tiquete valorVueloTiquete) {
        this.valorVueloTiquete = valorVueloTiquete;
    }

    public Tiquete getPuntosCompras() {
        return puntosCompras;
    }

    public void setPuntosCompras(Tiquete puntosCompras) {
        this.puntosCompras = puntosCompras;
    }

    public Tiquete getIdTiquetes() {
        return idTiquetes;
    }

    public void setIdTiquetes(Tiquete idTiquetes) {
        this.idTiquetes = idTiquetes;
    }
    
}
