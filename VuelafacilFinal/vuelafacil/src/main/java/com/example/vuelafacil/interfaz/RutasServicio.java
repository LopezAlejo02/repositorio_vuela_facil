/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.interfaz;

import com.example.vuelafacil.backend.Rutas;
import java.util.List;

/**
 *
 * @author FELIPE
 */
public interface RutasServicio {
    public Rutas save(Rutas rutas);
    public void delete(Integer id);
    public Rutas findById(Integer id);
    public List<Rutas> findAll();
    
}
