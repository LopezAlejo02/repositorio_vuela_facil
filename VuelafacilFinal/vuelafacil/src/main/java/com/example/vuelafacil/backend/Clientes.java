/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.backend;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author FELIPE
 */
@Entity
@Table(name="clientes")
public class Clientes implements Serializable {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="idCliente")
        private Integer idCliente;
        
        @Column(name="nombreCliente")
        private String nombreCliente;
        
        @Column(name="telefono")
        private Integer telefono;

        public Clientes() {
        }

        public Clientes(Integer idCliente, String nombreCliente, Integer telefono) {
            this.idCliente = idCliente;
            this.nombreCliente = nombreCliente;
            this.telefono = telefono;
        }
        
        

        public Integer getIdCliente() {
            return idCliente;
        }

        public void setIdCliente(Integer idCliente) {
            this.idCliente = idCliente;
        }

        public String getNombreCliente() {
            return nombreCliente;
        }

        public void setNombreCliente(String nombreCliente) {
            this.nombreCliente = nombreCliente;
        }

        public Integer getTelefono() {
            return telefono;
        }

        public void setTelefono(Integer telefono) {
            this.telefono = telefono;
        }
        
        
}
