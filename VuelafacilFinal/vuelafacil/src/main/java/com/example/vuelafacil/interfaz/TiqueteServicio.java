/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.interfaz;

import com.example.vuelafacil.backend.Tiquete;
import java.util.List;

/**
 *
 * @author FELIPE
 */
public interface TiqueteServicio {
    public Tiquete save(Tiquete tiquete);
    public void delete(Integer id);
    public Tiquete findById(Integer id);
    public List<Tiquete> findAll();
}
