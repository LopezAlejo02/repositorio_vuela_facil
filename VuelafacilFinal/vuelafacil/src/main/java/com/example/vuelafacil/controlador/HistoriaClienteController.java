/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.controlador;

import com.example.vuelafacil.backend.HistoriaCliente;
import com.example.vuelafacil.interfaz.HistoriaClienteServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author FELIPE
 */
@RestController
@CrossOrigin ("*")
@RequestMapping ("/historiacliente")
public class HistoriaClienteController {
    
    @Autowired
    private HistoriaClienteServicio  historiaClienteServicio;
    
     @PostMapping (value="/")
     public ResponseEntity<HistoriaCliente>agregar(@RequestBody HistoriaCliente historiacliente){
        HistoriaCliente obj=historiaClienteServicio.save(historiacliente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping (value="/list/{id}")
    public ResponseEntity<HistoriaCliente> eliminar(@PathVariable Integer id){
        HistoriaCliente obj = historiaClienteServicio.findById(id);
        if(obj!=null)
            historiaClienteServicio.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/list/{id}")
    public ResponseEntity<HistoriaCliente> editar(@RequestBody HistoriaCliente historiacliente){
        HistoriaCliente obj = historiaClienteServicio.findById(historiacliente.getIdHistoria());
        if(obj!=null)
        {
            obj.setIdClientes(historiacliente.getIdClientes());
            obj.setNombreClientes(historiacliente.getNombreClientes());
            obj.setNombreRutas(historiacliente.getNombreRutas());
            obj.setFechaHoraTiquete(historiacliente.getFechaHoraTiquete());
            obj.setValorVueloTiquete(historiacliente.getValorVueloTiquete());
            obj.setPuntosCompras(historiacliente.getPuntosCompras());
            obj.setIdTiquetes(historiacliente.getIdTiquetes());
            
                    
        historiaClienteServicio.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<HistoriaCliente> consultarTodo(){
    return historiaClienteServicio.findAll();
    }
    @GetMapping("/list/{id}")
    public HistoriaCliente consultaPorId(@PathVariable Integer id){
    return historiaClienteServicio.findById(id);
    }   
    
    
    
    
    
}
