/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.interfaz;

import com.example.vuelafacil.backend.Vuelos;
import java.util.List;

/**
 *
 * @author FELIPE
 */
public interface VuelosServicio {
    public Vuelos save(Vuelos vuelos);
    public void delete(Integer id);
    public Vuelos findById(Integer id);
    public List<Vuelos> findAll();
}
