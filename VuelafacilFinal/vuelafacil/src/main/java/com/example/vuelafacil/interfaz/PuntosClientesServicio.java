/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.interfaz;

import com.example.vuelafacil.backend.PuntosClientes;
import java.util.List;

/**
 *
 * @author FELIPE
 */
public interface PuntosClientesServicio {
    public PuntosClientes save(PuntosClientes puntosClientes);
    public void delete(Integer id);
    public PuntosClientes findById(Integer id);
    public List<PuntosClientes> findAll();
}
