/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios;

import com.example.vuelafacil.backend.Rutas;
import com.example.vuelafacil.interfaz.RutasServicio;
import com.example.vuelafacil.servicios.repositorio.RutasDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FELIPE
 */
@Service
public class RutasServiceImpl implements RutasServicio {
    
    @Autowired
    private RutasDAO rutasDao; 

    @Override
    @Transactional (readOnly = false)
    public Rutas save(Rutas rutas) {
        return rutasDao.save(rutas);
    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
        rutasDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public Rutas findById(Integer id) {
        return rutasDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<Rutas> findAll() {
        return (List<Rutas>) rutasDao.findAll();
    }
    
}
