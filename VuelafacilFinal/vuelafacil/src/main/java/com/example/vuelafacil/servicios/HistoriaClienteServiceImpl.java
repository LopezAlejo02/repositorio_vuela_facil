/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.servicios;

import com.example.vuelafacil.backend.HistoriaCliente;
import com.example.vuelafacil.interfaz.HistoriaClienteServicio;
import com.example.vuelafacil.servicios.repositorio.HistoriaClienteDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author FELIPE
 */
@Service
public class HistoriaClienteServiceImpl implements HistoriaClienteServicio{
    
    @Autowired
    private HistoriaClienteDAO historiaClienteDao;

    @Override
    @Transactional (readOnly = false)
    public HistoriaCliente save(HistoriaCliente historiacliente) {
        return historiaClienteDao.save(historiacliente);
    }

    @Override
    @Transactional (readOnly = false)
    public void delete(Integer id) {
        historiaClienteDao.deleteById(id);
    }

    @Override
    @Transactional (readOnly = true)
    public HistoriaCliente findById(Integer id) {
        return historiaClienteDao.findById(id).orElse(null);
    }

    @Override
    @Transactional (readOnly = true)
    public List<HistoriaCliente> findAll() {
        return (List<HistoriaCliente>) historiaClienteDao.findAll();
    }
    
}
