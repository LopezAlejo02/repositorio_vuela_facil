/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.vuelafacil.backend;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author FELIPE
 */

@Entity
@Table(name="vuelos")
public class Vuelos implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="idVuelo")
        private Integer idVuelo;
        
        @ManyToOne
        @JoinColumn(name="idRuta")
        private Rutas idRutas;
        
        @ManyToOne
        @JoinColumn(name="nombreRuta")
        private Rutas nombreRutas;
        
        @ManyToOne
        @JoinColumn(name="fechaHora")
        private Tiquete fechaHoraTiquete;
        
        @ManyToOne
        @JoinColumn(name="valorVuelo")
        private Tiquete valorVueloTiquete;

    public Vuelos() {
    }

    public Vuelos(Integer idVuelo, Rutas idRutas, Rutas nombreRutas, Tiquete fechaHoraTiquete, Tiquete valorVueloTiquete) {
        this.idVuelo = idVuelo;
        this.idRutas = idRutas;
        this.nombreRutas = nombreRutas;
        this.fechaHoraTiquete = fechaHoraTiquete;
        this.valorVueloTiquete = valorVueloTiquete;
    }

    public Integer getIdVuelo() {
        return idVuelo;
    }

    public void setIdVuelo(Integer idVuelo) {
        this.idVuelo = idVuelo;
    }

    public Rutas getIdRutas() {
        return idRutas;
    }

    public void setIdRutas(Rutas idRutas) {
        this.idRutas = idRutas;
    }

    public Rutas getNombreRutas() {
        return nombreRutas;
    }

    public void setNombreRutas(Rutas nombreRutas) {
        this.nombreRutas = nombreRutas;
    }

    public Tiquete getFechaHoraTiquete() {
        return fechaHoraTiquete;
    }

    public void setFechaHoraTiquete(Tiquete fechaHoraTiquete) {
        this.fechaHoraTiquete = fechaHoraTiquete;
    }

    public Tiquete getValorVueloTiquete() {
        return valorVueloTiquete;
    }

    public void setValorVueloTiquete(Tiquete valorVueloTiquete) {
        this.valorVueloTiquete = valorVueloTiquete;
    }
 
        
        
}
